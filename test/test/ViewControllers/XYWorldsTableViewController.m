//
//  XYWorldsTableViewController.m
//  test
//
//  Created by Maksym Lazebnyi on 10/18/15.
//  Copyright © 2015 xyrality. All rights reserved.
//

#import "XYWorldsTableViewController.h"
#import "World.h"

static NSString * const kCellIdentifier = @"WorldNameCell";

@interface XYWorldsTableViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation XYWorldsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.titleLabel setText: @"Worlds"];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.worldsDictionaries.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    NSDictionary *worldDict = [self.worldsDictionaries objectAtIndex:indexPath.row];
    World *world = [[World alloc] initWithDictionary:worldDict];
    
    [cell.textLabel setText:world.name];
    
    return cell;
}

#pragma mark - IBAction
- (IBAction)closeButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
