//
//  XYLoginViewController.m
//  test
//
//  Created by Maksym Lazebnyi on 10/17/15.
//  Copyright © 2015 xyrality. All rights reserved.
//

#import "XYLoginViewController.h"
#import "XYServerCommunication.h"
#import "XYWorldsTableViewController.h"

#define XYWeakSelf __weak typeof(self) weakSelf = self;
#define XYStrongSelf __strong typeof(weakSelf) strongSelf = weakSelf;

static NSString * const kSegueIdentifier = @"ToWorldsVC";

@interface XYLoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;
@property (weak, nonatomic) IBOutlet UIButton *goButton;

@end

@implementation XYLoginViewController

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController respondsToSelector:@selector(setWorldsDictionaries:)]) {
        XYWorldsTableViewController *worldsVC = (XYWorldsTableViewController *)segue.destinationViewController;
        [worldsVC setWorldsDictionaries:sender];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Private Methods
- (void)setActivityHidden:(BOOL)hidden {
    NSString *buttonTitle = @"Fetching worlds";
    if (hidden) {
        buttonTitle = @"Go";
    }
    [self.goButton setTitle:buttonTitle forState:UIControlStateNormal];
    
    [self.goButton setEnabled:hidden];
    [self.loginTextField setEnabled:hidden];
    [self.passwordTextField setEnabled:hidden];
    
    [self.activityView setHidden:hidden];
}

#pragma mark - IBActions
- (IBAction)goButtonTapped:(id)sender {
    [self setActivityHidden:NO];
    XYWeakSelf
    [XYServerCommunication getWorldsWithEmail:self.loginTextField.text password:self.passwordTextField.text completionHandler:^(id response, NSError *error) {
        [weakSelf setActivityHidden:YES];
        
        if (error) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            XYStrongSelf
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           [strongSelf dismissViewControllerAnimated:YES completion:nil];
                                       }];
            
            [alert addAction:okAction];
            [weakSelf presentViewController:alert animated:true completion:nil];
        } else {
            [weakSelf performSegueWithIdentifier:kSegueIdentifier sender:response];
        }
    }];
    
}

@end
