//
//  XYWorldsTableViewController.h
//  test
//
//  Created by Maksym Lazebnyi on 10/18/15.
//  Copyright © 2015 xyrality. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XYWorldsTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *worldsDictionaries;

@end
