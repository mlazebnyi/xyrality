//
//  XYServerCommunication.h
//  test
//
//  Created by Maksym Lazebnyi on 10/17/15.
//  Copyright © 2015 xyrality. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^XYRequestCompletionHandler)(id response, NSError *error);

@interface XYServerCommunication : NSObject

//Email and Password are required
+ (void)getWorldsWithEmail:(NSString *)email password:(NSString *)password completionHandler:(XYRequestCompletionHandler)handler;

@end
