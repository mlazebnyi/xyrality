//
//  XYWorldsRequests.m
//  test
//
//  Created by Maksym Lazebnyi on 10/17/15.
//  Copyright © 2015 xyrality. All rights reserved.
//

#import "XYWorldsRequests.h"

@implementation XYWorldsRequests

+ (void)getWorldsWithParameters:(NSDictionary *)parameters
                        success:(XYNetworkingSuccess)success
                        failure:(XYNetworkingFailure)failure {
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:wordsRequestString]];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[self httpBodyForParamsDictionary:parameters]];
    [request setTimeoutInterval:5];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (error && failure) {
                failure(error);
                return;
            }
            NSDictionary* plist;
            NSError *anError;
            if (data) {
                NSPropertyListFormat format;
                plist = [NSPropertyListSerialization propertyListWithData:data options:NSPropertyListImmutable format:&format error:&anError];
            }
            
            if (anError&&failure) {
                failure(anError);
            } else {
                success(plist);
            }
            
        });
        
    }] resume];
}

+ (NSData *)httpBodyForParamsDictionary:(NSDictionary *)paramDictionary {
    NSMutableArray *parameterArray = [NSMutableArray array];
    
    [paramDictionary enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
        NSString *param = [NSString stringWithFormat:@"%@=%@", key, [self percentEscapeString:obj]];
        [parameterArray addObject:param];
    }];
    
    NSString *string = [parameterArray componentsJoinedByString:@"&"];
    
    return [string dataUsingEncoding:NSUTF8StringEncoding];
}

+ (NSString *)percentEscapeString:(NSString *)string {
    NSString *result = CFBridgingRelease((__bridge CFTypeRef _Nullable)([string stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]));
    return [result stringByReplacingOccurrencesOfString:@" " withString:@"+"];
}


@end
