//
//  XYWorldsRequests.h
//  test
//
//  Created by Maksym Lazebnyi on 10/17/15.
//  Copyright © 2015 xyrality. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^XYNetworkingSuccess)(NSDictionary *responseDic);
typedef void (^XYNetworkingFailure)(NSError *error);

static NSString * const wordsRequestString = @"https://backend1.lordsandknights.com/XYRALITY/WebObjects/BKLoginServer.woa/wa/worlds";

@interface XYWorldsRequests : NSObject <NSURLConnectionDelegate>

+ (void)getWorldsWithParameters:(NSDictionary *)parameters
                              success:(XYNetworkingSuccess)success
                              failure:(XYNetworkingFailure)failure;

@end
