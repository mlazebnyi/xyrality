//
//  XYServerCommunication.m
//  test
//
//  Created by Maksym Lazebnyi on 10/17/15.
//  Copyright © 2015 xyrality. All rights reserved.
//

#import "XYServerCommunication.h"
#import "XYWorldsRequests.h"

@implementation XYServerCommunication

+ (void)getWorldsWithEmail:(NSString *)email password:(NSString *)password completionHandler:(XYRequestCompletionHandler)handler {
    
    NSString *deviceType = [NSString stringWithFormat:@"%@ - %@%@", [[UIDevice currentDevice] model], [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion]];
    NSString *deviceID = [[NSUUID UUID] UUIDString];
    
    NSDictionary *parameters = @{@"login" : email, @"password" : password, @"deviceType" : deviceType, @"deviceId" : deviceID};
    
    [XYWorldsRequests getWorldsWithParameters:parameters
                                               success:^(NSDictionary *responseDic) {
                                                   NSError *error = [self serverErrorHandler:responseDic[@"error"]];
                                                   
                                                   id response = responseDic[@"allAvailableWorlds"];
                                                   
                                                   if (![response isKindOfClass:[NSArray class]]) {
                                                       error = [NSError errorWithDomain:@"xyrality.com" code:500 userInfo:@{NSLocalizedDescriptionKey : @"Bad server response"}];
                                                   }
                                                   
                                                   handler(response, error);
                                                   
                                               } failure:^(NSError *error) {
                                                   handler(nil, error);
                                               }];
}

+ (NSError *)serverErrorHandler:(NSString *)responseError {
    
    if ([responseError isKindOfClass:[NSNull class]] || [responseError length] == 0) return nil;
    
    NSError *error = [NSError errorWithDomain:@"zalora.com"
                                         code:500
                                     userInfo:@{NSLocalizedDescriptionKey:NSLocalizedString(responseError, nil)}];
    return error;
}

@end
