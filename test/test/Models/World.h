//
//  World.h
//  test
//
//  Created by Maksym Lazebnyi on 10/18/15.
//  Copyright © 2015 xyrality. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const struct XYWorldFields {
    __unsafe_unretained NSString *name;
} XYWorldFields;

@interface World : NSObject

@property (nonatomic, copy) NSString *name;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
