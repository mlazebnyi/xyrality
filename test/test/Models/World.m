//
//  World.m
//  test
//
//  Created by Maksym Lazebnyi on 10/18/15.
//  Copyright © 2015 xyrality. All rights reserved.
//

#import "World.h"

const struct XYWorldFields XYWorldFields = {
    .name = @"name",
};

@implementation World

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    
    if (self) {
        [self loadWithDictionary:dictionary];
    }
    
    return self;
}

- (void)loadWithDictionary:(NSDictionary *)dictionary {
    self.name = [dictionary valueForKeyPath:XYWorldFields.name];
}

@end
