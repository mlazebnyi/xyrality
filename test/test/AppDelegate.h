//
//  AppDelegate.h
//  test
//
//  Created by Maksym Lazebnyi on 10/17/15.
//  Copyright © 2015 xyrality. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

